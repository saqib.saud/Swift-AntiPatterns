Both Delegation pattern and Observer patterns are very important patterns and whole cocoa framework relies on this. It is important to understand the purpose of both patterns before we utilise them. Generally Delegation pattern is the way to go. It is type safe and does not require copy/pasting code.



Bad practice
-------------

```Objc
 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveVidyoClientOutEventSignedInNotification:) name:k_NOTIFICATION_CLIENT_OUT_EVENT_SIGNED_IN object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveVidyoClientOutEventSignedOutNotification:) name:k_NOTIFICATION_CLIENT_OUT_EVENT_SIGNED_OUT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveVidyoClientOutEventConferenceEndedNotification:) name:k_NOTIFICATION_CLIENT_OUT_EVENT_CONFERENCE_ENDED object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveVidyoClientOutEventSignedOutNotification:) name:"String name of notification" object:nil];
    
- (void)dealloc
{
    NSLog(@"Function %s",__FUNCTION__);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:k_NOTIFICATION_CLIENT_OUT_EVENT_SIGNED_IN object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:k_NOTIFICATION_CLIENT_OUT_EVENT_SIGNED_OUT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:k_NOTIFICATION_CLIENT_OUT_EVENT_CONFERENCE_ENDED object:nil];
}
    
```




Good practice
--------------

Delegation Pattern is a safe choice in most of the cases. It need to assesses case to case basis. 

In case you have to use observer pattern, make sure you are not using `string` names. Swift enums can help here.
