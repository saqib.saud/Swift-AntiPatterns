Creating UITableViewCells
=========================

```swift
public func registerClass(cellClass: AnyClass?, forCellWithReuseIdentifier identifier: String)
public func registerNib(nib: UINib?, forCellWithReuseIdentifier identifier: String)
public func dequeueReusableCellWithIdentifier(identifier: String, forIndexPath indexPath: NSIndexPath) -> UITableViewCell
```
One thing common in above methods is *identifier*. The identifier should not be part of viewcontroller, the *Cell* itself should provide the identifier. This way cell class will be loosely coupled with viewcontroller and we can swap it any time without any effort. 

Bad practice
-------------

```swift

class BookListViewController: UIViewController, UICollectionViewDataSource {

    @IBOutlet private weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "BookCell", bundle: nil)
        self.collectionView.registerNib(nib, forCellWithReuseIdentifier: "BookCell")
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("BookCell", forIndexPath: indexPath)
    
        if let bookCell = cell as? BookCell {
            // TODO: configure cell
        }
    
        return cell
    }
}

```

Good practice
--------------

Use protocols to provide cell identifiers, all cells should adhere to this protocol. Guille Gonzalez [explains this concept in detail on his blog](https://medium.com/@gonzalezreal/ios-cell-registration-reusing-with-swift-protocol-extensions-and-generics-c5ac4fb5b75e#.xh8h4wczi). 

Relevant: [You can use same concept for UIViewController too.](https://medium.com/swift-programming/uistoryboard-safer-with-enums-protocol-extensions-and-generics-7aad3883b44d#.4wao3che0)

Configuring UITableViewCells
============================
```swift 
func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
```

Do not configure and customize cell in the above method. 

Bad practice
-------------

code snippt from app published at appstore

```objc

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CreditCardDetailCell *cell = (DetailCell *)[tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DetailCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSDictionary *dict = [_listOfCreditCards objectAtIndex:indexPath.row];
    
    id nameOnCreditCard = [dictCreditCard objectForKey:@"name"];
    NSString *strName = ([Common isStringEmptyOrNull:name] == YES) ? @"(no name)" : (NSString *)nameOnCreditCard;
    cell.lblName.text = strName;
    
    NSString *strExpiryDate = [NSString stringWithFormat:@"Expiry: %li / %li", (long)[[dict objectForKey:@"exp_month"] integerValue], (long)[[dict objectForKey:@"exp_year"] integerValue]];
    cell.lblExpiryDate.text = strExpiryDate;
    
    cell.lblCardType.text = [dict objectForKey:@"brand"];
    
    if ([[dict objectForKey:@"id"] isEqualToString:_selectedCreditCard])
        [cell.btnRadioSelection setSelected:YES];
    else
        [cell.btnRadioSelection setSelected:NO];
    
    return cell;
}
```

Good practice
--------------

```swift
override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("earthquakeCell", forIndexPath: indexPath) as! EarthquakeTableViewCell
        
    if let earthquake = fetchedResultsController?.objectAtIndexPath(indexPath) as? Earthquake {
        cell.configure(earthquake)
    }

    return cell
}
```
